---
  title: GitLabを用いた継続的なソフトウェアコンプライアンス
  description: GitLabを使用して、安全なソフトウェアサプライチェーンとともに、規制に準拠したアプリケーションを構築する方法。
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  コンプライアンスを自動化してリスクを軽減
        title: GitLabを用いた継続的なソフトウェアコンプライアンス
        subtitle: 安全なソフトウェアサプライチェーンとともに、一般的な規制基準を満たすアプリケーションを構築しましょう。
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimateを無料で試す
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: 価格設定を確認
          url: /pricing/
          data_ga_name: pricing
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          image_url_mobile: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          alt: "画像：公共部門向けのGitLab"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: お客様：
        logos:
          - name: Duncan Aviation Logo
            image: "/nuxt-images/logos/duncan-aviation-logo.svg"
            url: /customers/duncan-aviation/
            aria_label: Duncan Aviationの事例へのリンク
          - name: Curve Logo
            image: /nuxt-images/logos/curve-logo.svg
            url: /customers/curve/
            aria_label: Curveの顧客事例へのリンク
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Hiltiの顧客事例へのリンク
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: The Zebraの顧客事例へのリンク
          - name: New 10 Logo
            image: /nuxt-images/logos/new10-logo.svg
            url: /customers/new10/
            aria_label: Conversicaの顧客事例へのリンク
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus-logo.svg
            url: /customers/chorus/
            aria_label: Bendigo and Adelaide Bankの顧客事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: 機能
          href: '#capabilities'
        - title: 顧客
          href: '#customers'
        - title: 価格設定
          href: '#pricing'
        - title: リソース
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: ソフトウェアコンプライアンスの簡素化と自動化
                image:
                  image_url: "/nuxt-images/solutions/compliance/compliance-overview.png"
                  alt: GitLabを用いた継続的なソフトウェアコンプライアンス
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: リスクの管理
                    text: ただ単にコード内のセキュリティ上の欠陥を減らすだけではありません
                  - icon:
                      name: clipboard-check
                      alt: チェックマーク付きのクリップボードアイコン
                      variant: marketing
                    header: シンプルかつスムーズ
                    text: 統一された方法でコンプライアンスを定義、実施、およびレポートできます
                  - icon:
                      name: release
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    header: ガードレールの実装
                    text: アクセスを制御し、ポリシーを実装します
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                markdown: true
                subtitle: 高速かつ安全にコンプライアンスを実現
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/compliance/compliance-benefits.png
                solutions:
                  - title: ポリシー管理
                    description: コンプライアンスフレームワークと一般的な規制を遵守するために、ルールとポリシーを定義します
                    list:
                      - "**きめ細かなユーザーの役割と権限：**組織に適したユーザーの役割と権限レベルを定義する"
                      - "**アクセス制御：** 2要素認証と有効期限トークンでアクセスを制限する"
                      - "**コンプライアンス設定：**特定のプロジェクト、グループ、およびユーザーのコンプライアンスポリシーを定義し、実施する"
                      - "**認証情報のインベントリ：** GitLab Self-Managedインスタンスへのアクセスに使用できるすべての認証情報を追跡する"
                      - "**保護ブランチ：**適切な権限または承認なしに、ブランチの作成、プッシュ、削除を含む、特定のブランチへの不正な変更を制御する"
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*1r05yn6*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU1ODM3LjAuMC4w#policy-management
                    data_ga_name: policy mnagement
                    data_ga_location: solutions block
                  - title: コンプライアンスに準拠したワークフローの自動化
                    description: 全体的なビジネスリスクを軽減しながら、定義されたルール、ポリシー、および職務分掌を実施します
                    list:
                      - "**コンプライアンスフレームワークプロジェクトのテンプレート：**監査証跡を維持し、コンプライアンスプログラムを管理するために役立つ、HIPAAなどの特定の監査プロトコルに対応するプロジェクトを作成する"
                      - "**コンプライアンスフレームワークのプロジェクトラベル：**ラベルによって、プロジェクトに共通のコンプライアンス設定を簡単に適用する"
                      - "**コンプライアンスフレームワークパイプライン：**すべてのパイプラインで実行する必要があるコンプライアンスジョブを定義し、セキュリティスキャンが実行され、アーティファクトが作成および保存され、または組織の要求事項により必要とされるその他のステップが確実に行われるようにする"
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*nbfxzt*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU2NDIyLjAuMC4w#compliant-workflow-automation
                    data_ga_name: workflow automation
                    data_ga_location: solutions block
                  - title: 監査管理
                    description: 監査データに簡単にアクセスして、監査の準備をし、イシューの根本原因をよりよく理解します
                    list:
                      - "**[監査イベント：](https://docs.gitlab.com/ee/administration/audit_events.html)**ユーザーの権限レベルの変更、新規ユーザーの追加者、ユーザーの削除者などの重要なイベントを追跡する"
                      - "**[監査イベントのストリーミング：](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)**選択したツールに監査ログを統合する"
                      - "**[監査レポート：](https://docs.gitlab.com/ee/administration/audit_reports.html)**インスタンス、グループ、プロジェクトイベント、なりすましデータ、サインイン、ユーザーイベントなどに関する包括的なレポートを生成して監査人に対応する"
                      - "**[コンプライアンスレポート：](https://docs.gitlab.com/ee/user/compliance/compliance_report/)**マージリクエストにおけるコンプライアンス違反、違反の理由、および違反の重要度の概要を把握する"
                  - title: 脆弱性と依存関係の管理
                    description: アプリケーションの脆弱性と依存関係を表示し、トリアージし、傾向を示し、追跡し、そして解決します
                    list:
                      - "**[セキュリティダッシュボード：](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)**現在のセキュリティステータスを示すアプリケーションにアクセスし、修復を開始する"
                      - "**[ソフトウェア部品表：](https://docs.gitlab.com/ee/user/application_security/dependency_list/)**アプリケーションとコンテナの依存関係をスキャンしてセキュリティ上の欠陥を確認し、使用されている依存関係のソフトウェア部品表（SBOM）を作成する"
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  企業に信頼され 
                  <br />
                  開発者が愛用
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/duncan-aviation-logo.svg
                      alt: Duncan Aviationロゴ
                    quote: GitLabを活用して、パイプラインを用いて手動プロセスを自動化できました。今では定期的にコードをデプロイし、重要な変更や修正をより迅速にお客様に提供できるようになりました。
                    author: Ben Ferguson氏
                    position: Duncan Aviation社シニアプログラマー
                    ga_carousel: duncan aviation testimonial
                    url: /customers/duncan-aviation/
                  - title_img:
                      url: /nuxt-images/logos/curve-logo.svg
                      alt: Curveロゴ
                    quote: GitLabに移行する前は、運用チームに大きな負担がかかっていました。開発者が効率的に仕事をこなせるようにするために戦っていました。すべてを1か所にまとめ、色々な要素を一目でうまく管理できるようにすることが、私たちにとって当然の選択でした。
                    author: Ed Shelto氏
                    position: Curve社サイト信頼性エンジニア
                    ga_carousel: curve testimonial
                    url: /customers/curve/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hiltiロゴ
                    quote: GitLabはスイートのようにバンドルされており、非常に洗練されたインストーラを搭載しています。そしてどういうわけかうまく機能します。これは、とにかく立ち上げて稼動させたい企業にとっては非常にありがたいことです。
                    author: Daniel Widerin氏
                    position: Hilti社ソフトウェアデリバリー主任
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: 最適なプランを見つけましょう
                cta:
                  url: /pricing/
                  text: 価格設定の詳細はこちら
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: 価格設定
                tiers:
                  - id: free
                    title: Free
                    items:
                      - 静的アプリケーションセキュリティテスト（SAST）とシークレット検出
                      - JSONファイルの調査
                    link:
                      href: /pricing/
                      text: 詳細はこちら
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: Freeプラン
                  - id: premium
                    title: Premium
                    items:
                      - 静的アプリケーションセキュリティテスト（SAST）とシークレット検出
                      - JSONファイルの調査
                      - MRの承認とその他の一般的な制御
                    link:
                      href: /pricing/
                      text: 詳細はこちら
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: Premiumプラン
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Premiumの機能に加え、以下の機能が含まれます
                      - 包括的なセキュリティスキャナー（SAST、DAST、秘密、Dependency Scanning、コンテナ、IaC、API、クラスターイメージ、ファジングテストなど）
                      - MRパイプライン内での実行可能な結果
                      - コンプライアンスパイプライン
                      - セキュリティとコンプライアンスのダッシュボード
                      - その他たくさん
                    link:
                      href: /pricing/
                      text: 詳細はこちら
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimateプラン
                    cta:
                      href: /free-trial/
                      text: Ultimateを無料で試す
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimateプラン
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: 関連リソース
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: 動画アイコン
                  event_type: "動画"
                  header: コンプライアンスパイプライン
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
                  href: https://www.youtube.com/embed/jKA_e_jimoI
                  data_ga_name: Compliant pipelines
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: 動画アイコン
                  event_type: "動画"
                  header: 継続的なソフトウェアコンプライアンス
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/compliance/continuous-software-compliance.jpeg"
                  href: https://player.vimeo.com/video/694891993?h=7768f52e29
                  data_ga_name: Continuous software compliance
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: 動画アイコン
                  event_type: "動画"
                  header: SBOMと認証
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: eBookのアイコン
                  event_type: "eBook"
                  header: "ソフトウェアサプライチェーンのセキュリティガイド"
                  link_text: "詳細はこちら"
                  image: "/nuxt-images/blogimages/modernize-cicd.jpg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: eBookのアイコン
                  event_type: "eBook"
                  header: "GitLab DevSecOpsアンケート"
                  link_text: "詳細はこちら"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログのアイコン
                  event_type: "ブログ"
                  header: "DevOpsにおけるコンプライアンスの重要性"
                  link_text: "詳細はこちら"
                  href: https://about.gitlab.com/blog/2022/08/15/the-importance-of-compliance-in-devops/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "The importance of compliance in DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログのアイコン
                  event_type: "ブログ"
                  header: "GitLabで活用すべき上位5つのコンプライアンス機能"
                  link_text: "詳細はこちら"
                  href: https://about.gitlab.com/blog/2022/07/13/top-5-compliance-features-to-leverage-in-gitlab/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Top 5 compliance features to leverage in GitLab"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: ブログアイコン
                    variant: marketing
                  event_type: "ブログ"
                  header: "職務分掌を実施し、コンプライアンスを達成する方法"
                  link_text: "詳細はこちら"
                  href: "https://about.gitlab.com/blog/2022/04/04/ensuring-compliance/"
                  image: /nuxt-images/blogimages/scm-ci-cr.png
                  data_ga_name: "How to enforce separation of duties and achieve compliance"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: ブログアイコン
                    variant: marketing
                  event_type: "ブログ"
                  header: "DevOpsの監査について知っておく必要のあること"
                  link_text: "詳細はこちら"
                  href: "https://about.gitlab.com/blog/2022/08/31/what-you-need-to-know-about-devops-audits/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "What you need to know about DevOps Audits"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: レポートアイコン
                    variant: marketing
                  event_type: "アナリストレポート"
                  header: "GitLabが『2022 Gartner Magic Quadrant』で挑戦者に認定"
                  link_text: "詳細はこちら"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: GitLabでさらに多くのことを実現
        column_size: 4
        link :
          url: /solutions/
          text: その他のソリューションを確認
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLabは、ソフトウェアデリバリーを自動化し、エンドツーエンドのソフトウェアサプライチェーンを保護することで、スピードとセキュリティの両立を可能にします。
            icon:
              name: devsecops
              alt: DevSecOpsアイコン
              variant: marketing
            href: /solutions/security-compliance/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: ソフトウェアサプライチェーンのセキュリティ
            description: ソフトウェアサプライチェーンの安全性とコンプライアンスを保証します。
            icon:
              name: continuous-delivery
              alt: 継続的デリバリー
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body
          - title: ソフトウェアデリバリーの自動化
            description: デジタルイノベーション、クラウドネイティブトランスフォーメーション、アプリケーションのモダナイゼーションを達成するためには自動化が不可欠です。
            icon:
              name: continuous-integration
              alt: 継続的インテグレーションアイコン
              variant: marketing
            href: /solutions/delivery-automation/
            data_ga_name: automated software delivery learn more
            data_ga_location: body