---
  title: GitLab CI/CD für GitHub
  description: Mit der GitHub-Integration können GitLab-Benutzer(innen) jetzt ein CI/CD-Projekt in GitLab erstellen, das mit einem externen GitHub.com- oder GitHub Enterprise-Code-Repository verbunden ist!
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab CI/CD für GitHub
        subtitle: Code auf GitHub hosten. Erstellen, testen und bereitstellen auf GitLab.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html
          text: Dokumentation
          data_ga_name: github integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab CI/CD für GitHub"
    - name: copy
      data:
        block:
          - header: Build und Test automatisieren
            id: automate-build-and-test
            text: |
              Mit der GitHub-Integration können GitLab-Benutzer(innen) jetzt ein CI/CD-Projekt in GitLab erstellen, das mit einem externen GitHub.com- oder GitHub Enterprise-Code-Repository verbunden ist. Dadurch wird GitLab CI/CD automatisch ausgeführt, wenn Code an GitHub gepusht wird. Die CI/CD-Ergebnisse werden nach Abschluss sowohl an GitLab als auch an GitHub zurückgesendet.
    - name: copy-media
      data:
        block:
          - header: Für wen eignet sich GitLab CI/CD für GitHub?
            id: who-is-git-lab-ci-cd-for-git-hub-for
            text: |
              ### Open source projects
              Wenn du ein öffentliches Open-Source-Projekt auf GitHub hast, kannst du jetzt die Vorteile von kostenlosem CI/CD auf GitLab.com nutzen. Im Rahmen unseres Engagements für Open-Source bieten wir allen öffentlichen Projekten unsere Funktionen des höchsten Tarifs (GitLab SaaS Ultimate) kostenlos an. Während andere CI/CD-Anbieter dich auf eine Handvoll gleichzeitiger Jobs beschränken, bietet [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="open source projects CI"} Open-Source-Projekten Hunderte von gleichzeitigen Jobs mit 50.000 kostenlosen Rechenminuten.

              ### Large Enterprises
              Unsere größten Kunden erzählen uns, dass sie oft viele Teams haben, die viele verschiedene Tools verwenden. Sie möchten GitLab für CI/CD standardisieren, aber der Code ist in GitLab, GitHub und anderen Repositorys gespeichert. Diese Funktion ermöglicht es Unternehmen jetzt, gemeinsame CI/CD-Pipelines für alle ihre verschiedenen Repositorys zu verwenden. Dies ist eine wichtige Zielgruppe und der Grund, warum wir CI/CD für GitHub zu einem Teil unseres selbstverwalteten Premium-Plans gemacht haben.

              ### Anyone using GitHub&#46;com
              Obwohl GitLab für die Verwendung von SCM und CI/CD in derselben Anwendung entwickelt wurde, verstehen wir den Reiz der Verwendung von GitLab CI/CD mit der Versionskontrolle von GitHub. Deshalb bieten wir für das nächste Jahr die GitLab CI/CD für GitHub-Funktion als Teil unseres kostenlosen [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="anyone using github.com"} Tarifs an. Das bedeutet, dass jeder, der GitHub nutzt – von persönlichen Projekten über Startups bis hin zu KMUs – GitLab CI/CD kostenlos nutzen kann. Ab 400 kostenlosen Rechenminuten können Benutzer(innen) auch [ihre eigenen Runner hinzufügen](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-specific-runner){data-ga-name="add runners" data-ga-location="body"} oder Tarife upgraden, um mehr zu erhalten.

              ### Gemnasium customers
              Wir haben kürzlich [Gemnasium erworben](/press/releases/2018-01-30-gemnasium-acquisition.html){data-ga-name="gemnasium" data-ga-location="body"}. Wir freuen uns sehr darüber, dass ein so großartiges Team zu uns stößt, aber wir wollen uns auch um die Leute kümmern, die Gemnasium benutzt haben, und ihnen einen Migrationspfad anbieten. Im Rahmen unseres integrierten Sicherheitsscans haben wir bereits [Gemnasium-Funktionen](/releases/2018/02/22/gitlab-10-5-released/#gemnasium-dependency-checks){data-ga-name="gemnasium features" data-ga-location="body"} bereitgestellt. Mit GitLab CI/CD für GitHub können Gemnasium-Kunden, die GitHub und Gemnasium verwendet haben, GitLab CI/CD für ihre Sicherheitsanforderungen verwenden, ohne ihren Code migrieren zu müssen.
            image:
              image_url: /nuxt-images/logos/github-logo.svg
              alt: ""
          - header: Vorteile
            id: benefits
            inverted: true
            text: |
              Mit GitLab CI/CD für GitHub können Benutzer(innen) ein CI/CD-Projekt in GitLab erstellen, das mit einem externen GitHub-Code-Repository verbunden ist. Dadurch werden mehrere Komponenten automatisch konfiguriert:

              * [Pull-Spiegelung](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository){data-ga-name="pull mirroring" data-ga-location="body"} des Repositorys.
              * Ein Push-Webhook an GitLab löst CI/CD sofort aus, sobald ein Code übergeben wurde.
              * Die Integration des GitHub-Projektdienstes überträgt den CI-Status per Webhook zurück an GitHub.
            image:
              image_url: /nuxt-images/features/github-status-github.png
              alt: ""
          - header: GitLab CI/CD für externe Repositorys
            text: |
              Nicht nur GitLab lässt sich mit GitHub integrieren – du kannst auch CI/CD von jedem externen Git Repository eines beliebigen Anbieters ausführen, indem du ein Repository per URL zu deinem Projekt hinzufügst und einen Webhook konfigurierst. Zum Beispiel kannst du [Bitbucket so konfigurieren, dass es GitLab CI/CD verwendet](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/bitbucket_integration.html){data-ga-name="bitbucket for gitlab" data-ga-location="body"}.

              Lies dazu die Dokumentation für [GitLab CI/CD für externe Repositorys](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/){data-ga-name="ci/cd for external repositories" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/git-logo.svg
              alt: ""
          - header: Tarife und Preise
            id: plans-and-pricing
            inverted: true
            text: |
              GitLab CI/CD für GitHub wird nicht separat berechnet, sondern ist Teil des GitLab-Standardprodukts.

              Für **selbstverwaltete Installationen** ist GitLab CI/CD für GitHub für Kunden mit den Tarifen **Premium** und **Ultimate** verfügbar.

              GitLab CI/CD für GitHub ist bis zum 22. März 2020 in unserem **kostenlosen** Tarif verfügbar. (Nach dem 22. März 2020 wird diese Funktion in den Tarif **Premium** verschoben und steht nur noch Benutzern mit Lizenzen der Tarife **Premium** und **Ultimate** zur Verfügung.)

              Weitere Informationen zu den GitLab-Abonnementoptionen findest du auf der [Preisseite](/pricing/){data-ga-name="pricing" data-ga-location="body"}.
            icon:
              name: checklist
              alt: Checkliste
              variant: marketing
              hex_color: '#F43012'
          - header: Mehr erfahren
            id: learn-more
            text: |
              * GitLab CI/CD: Erfahre mehr über die [Vorteile von GitLab CI/CD](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}.
              * Dokumentation: Die [Dokumentation](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html){data-ga-name="documentation" data-ga-location="body"} hilft dir bei den ersten Schritten.
              * Veröffentlichung: Lies den Beitrag zum Release für [GitLab 10.6](/releases/2018/03/22/gitlab-10-6-released/){data-ga-name="gitlab 10.6" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/gitlab-logo.svg
              alt: ""
