title: Environmental, Social and Governance at GitLab
description: Deeply integrated into our business philosophy, GitLab’s ESG strategy is driven by our values of Collaboration, Results, Efficiency, Diversity, Inclusion and Belonging, Iteration, and Transparency (CREDIT). Read the full report in our handbook.
hero: 
  header: Environmental, Social and Governance (ESG) at GitLab
  description:
    - We’re thrilled to share our inaugural ESG report.  
    - Deeply integrated into the business philosophy, GitLab’s ESG strategy is driven by our values of Collaboration, Results, Efficiency, Diversity, Inclusion and Belonging, Iteration, and Transparency (CREDIT). Read the full report in our [handbook](/handbook/esg/){data-ga-name="esg handbook" data-ga-location="hero"}.
    - Download our Fiscal Year 2023 (FY23) ESG Report Highlights
  image:
    src: /nuxt-images/esg/esg-hero.png
    alt: Teammates collaborating
  button:
    text: Download
    href: /files/esg/GitLab_ESG_FY23_Highlights.pdf
    icon:
      variant: product
      name: download
letter: 
  author: 
    name: Sid
    role: GitLab Co-Founder and CEO
    image:
      src: /nuxt-images/esg/sid-portrait.png
      alt: Sid portrait
  text:
    - Welcome to GitLab’s inaugural Environmental, Social, and Governance (ESG) report. ESG has been an integral part of GitLab’s business and culture, even before we explicitly used the term “ESG.” Our approach to ESG aligns with our Values. This can be seen in the management and oversight of our business and in the way we have managed risks and opportunities related to our team and our wider community. It is also evident in our customer-centric approach, our all remote philosophy, the development of our products, and our being an intentional force for good.

    - GitLab has been a remote workplace since day one. That means we work to eliminate geographical constraints by relying on our scalable employment solution when it comes to recruiting our team members. We focus on skills, experience, enthusiasm, energy, and ability to get the job done well. This has proven to be a win-win, opening doors for us at GitLab and for those who want to work with us. Building an all-remote company is just one example of how we’ve authentically incorporated ESG principles into our structure and operations. The intentional investments we’ve made in ESG have been a natural extension of how we started.

    - Without it being formalized, ESG was already part of our core purpose to help people increase their lifetime earnings through access to opportunities and the DevSecOps platform. That said, we are excited to invest more in this area. This report offers ESG information and data in a single location in an effort to enhance our transparency and clarity. 

    - |
      In 2022, we completed our first materiality assessment in which we engaged with internal and external stakeholders to understand where we should prioritize our ESG efforts. Our materiality assessment uncovered the six ESG topics we will focus on to optimize results. We have since formalized our ESG program, identified areas of opportunity, and established a strategy to address and manage the key ESG priorities. This report dives into each of those key ESG priorities and shares how we’ve taken action.

      We look forward to building on our efforts to date and continuing to share progress with our stakeholders.

      Thank you, 
      ![Sid signature](/nuxt-images/esg/sid-signature.png)
      GitLab Co-Founder and CEO
highlights: 
  header: FY23 ESG Highlights
  highlights:
    - image: 
        src: /nuxt-images/esg/windmill.png
        alt: windmill logo
      text: We conducted our first greenhouse gas inventory. In FY23, GitLab’s net corporate emissions were 16,654 metric tons of CO₂e
    - image: 
        src: /nuxt-images/esg/gitlab-elevate-badge-charcoal-rgb.png
        alt: gitlab elevate
      text: Launched [Elevate](/handbook/people-group/learning-and-development/elevate/){data-ga-name="elevate handbook" data-ga-location="body"}, our leadership development course to upskill managers
    - image: 
        src: /nuxt-images/esg/womens-history-month-2023-assets_browser-whm.png
        alt: women's history month
      text: We beat our representation goal by 7% for Women in Senior Leadership
    - image: 
        src: /nuxt-images/esg/GitLab_Foundation_Logo_Vertical_Color@1x.png
        alt: gitlab foundation logo
      text: The [GitLab Foundation](https://www.gitlabfoundation.org/) launched with a mission focused on supporting individuals, families, and communities to grow their lifetime earnings through education, training, access to opportunities, and systems change on a global scale
    - image: 
        src: /nuxt-images/esg/cloud-security 2.png
        alt: cloud security icon
      text: We received our ISO 27001 certification to include ISO 27017:2015 cloud security standard, and ISO 27018:2019 privacy standard
    - image: 
        src: /nuxt-images/esg/gitlab-marketing-icon_agile-96px 1.png
        alt: infinity loop
      text: We improved the developer experience with AI, by investing in AI through the UnReview acquisition, and by launching Suggested Reviewers, and AI Assisted Code Suggestions, which is GitLab’s first AI powered features which help improve developer productivity and efficiency all within a single application
links: 
  - title: Overview
    href: '#overview'
  - title: Environment
    href: '#envitonment'
  - title: Social
    href: '#social'
  - title: Governance
    href: '#governance'
showcase:
  overview:
    textVariant: heading4
    text:
      description: In 2022, we completed a materiality assessment to determine which ESG topics are most important to our business and our stakeholders. Through engagement with both internal and external stakeholders, we explored which ESG topics have the greatest impact on GitLab’s business, and where we have the potential to have the greatest impact on people, society and the environment. Six topics rose to the top.
  sections:
    - name: Environment
      purpleBackground: true
      blocks:
        - header: Greenhouse Gas Emissions
          description: Part of doing responsible business means minimizing our environmental footprint. In May 2023, we completed our first greenhouse gas (GHG) inventory. We will use the results of the inventory to better understand our key sources of emissions, set reduction goals using FY23 as a baseline, develop a reduction plan, and educate our fully remote team on how they can understand and reduce their GHG emissions at home.
          image: 
            src: /nuxt-images/esg/environment-1.png
            alt: bottom view of trees and buildings
    - name: Social
      purpleBackground: false
      blocks:
        - header: Diversity, Inclusion and Belonging (DIB)
          description: DIB is fundamental to the success of GitLab and as such, is one of our core values. We incorporate the value of Diversity, Inclusion, and Belonging into all that we do – it’s not just an aspect of GitLab, it is GitLab. As a global company, we strive for a team that is representative of our users. As such, we aim to create a work environment that is transparent in nature and fosters a space in which everyone is welcomed. Read GitLab's [DIB report](/diversity-inclusion-belonging/){data-ga-name="DIB report" data-ga-location="body"}.
          image: 
            src: /nuxt-images/esg/social-1.png
            alt: meeting
        - header: Talent and Engagement
          description: "We're a team of helpful, passionate people who want to see each other, GitLab, and the broader GitLab community succeed. We care about what our team members achieve: the code shipped, the user that was made happy, and the team member that was helped."
          image: 
            src: /nuxt-images/esg/social-2.png
            alt: woman working on a laptop
        
    - name: Governance
      purpleBackground: true
      blocks:
        - header: Information Security and Privacy
          description: At GitLab, we know how much security and privacy matter to our customers and stakeholders. We maintain a formal [Security Assurance](/handbook/security/security-assurance/) department responsible for monitoring and reporting on GitLab's compliance with various security frameworks and standards. For more information on our approach to information security and data privacy, please visit our [Trust Center](/security/){data-ga-name="security" data-ga-location="body"}.
          image: 
            src: /nuxt-images/esg/governance-1.png
            alt: coworkers discussing work lookingg at a screen
        - header: Responsible Product Development
          description: GitLab's [product mission](/handbook/product-development-flow/) is to consistently create products and experiences that users love and value. Responsible product development is integral to this mission. We are committed to secure and ethical operations as an organization and, beyond that, strive to set an example by empowering our wider GitLab community to build and work with the highest levels of security through our DevSecOps platform.
          image: 
            src: /nuxt-images/esg/governance-2.png
            alt: woman writing on whiteboard
        - header: Business Ethics
          description: GitLab is committed to the highest standards of legal and ethical business conduct and has long operated its business consistent with written operating principles and policies that reinforce this commitment. 
          image: 
            src: /nuxt-images/esg/governance-3.png
            alt: meeting room
learn_more:
  header: 'Learn more about GitLab’s FY23 ESG efforts:'
  cards:
    - title: Read the full report in our handbook
      icon: report-alt-2
      type: Report
      href: /handbook/esg/
    - title: Download our ESG Highlights
      icon: report-alt-2
      type: Report Highlights
      href: /files/esg/GitLab_ESG_FY23_Highlights.pdf
    - title: View our Performance Data Tables
      icon: report-alt-2
      type: Data
      href: /files/esg/PERFORMANCE_DATA_TABLES_FV.pdf
    - title: View our GRI Index
      icon: report-alt-2
      type: Data
      href: /files/esg/GRI_CONTENT_INDEX_FV.pdf
    - title: View our SASB Index
      icon: report-alt-2
      type: Data
      href: /files/esg/SASB_INDEX_FV.pdf
    