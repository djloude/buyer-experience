import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';

export class AnalyticsAndInsightsService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    COPY: 'copy',
    CUSTOMER_SUCCESS_CARD: 'analytics-insights-customer-success-card',
    HERO: 'solutions-hero',
    REPORT_CTA: 'report-cta',
    FEATURED: 'analytics-insights-featured',
    SHOWCASE_FAQ: 'analytics-insights-showcase-faq',
    SIDE_NAVIGATION: 'side-navigation-variant',
    RESOURCE_CAROUSEL: 'solutions-resource-carousel',
  };

  private readonly componentIds = {
    BENEFITS: 'benefits',
    CAPABILITIES: 'capabilities',
    CUSTOMERS: 'customers',
    RECOGNITIONS: 'recognitions',
    RESOURCES: 'resources',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /solutions/analytics-and-insights page
   * @param slug
   */
  async getContent(slug: string) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    // Default locale data - Contentful
    if (isDefaultLocale) {
      try {
        return this.getContentfulData(slug);
      } catch (e) {
        throw new Error(e);
      }
    }

    try {
      const content = this.$ctx
        .$content(this.$ctx.i18n.locale, `${slug}/index`)
        .fetch();

      // Localized data - YML files
      return content;
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    try {
      const content = await getClient().getEntries({
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      });

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [freeTrial] = content.items;

      return this.transformContentfulData(freeTrial);
    } catch (e) {
      throw new Error(e);
    }
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    const sideNav: any = mappedContent.filter(
      (component) => component.name === this.componentNames.SIDE_NAVIGATION,
    )[0];

    const hero: any = mappedContent.filter(
      (component) => component.name === this.componentNames.HERO,
    )[0];

    const otherComponents = mappedContent.filter(
      (component) =>
        component.name !== this.componentNames.HERO &&
        component.name !== this.componentNames.SIDE_NAVIGATION,
    );

    const newSideNav = {
      name: this.componentNames.SIDE_NAVIGATION,
      slot_enabled: true,
      links: sideNav.links,
      slot_content: [...otherComponents],
    };

    const newArray = [];
    const newCopy = hero.copy;
    delete hero.copy;

    newArray.push(hero);
    newArray.push(newCopy);
    newArray.push(newSideNav);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      template: 'industry',
      next_step_alt: true,
      no_gradient: true,
      components: newArray,
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      const mappedComponent = this.mapCtfComponent(ctfComponent);
      components.push(mappedComponent);
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHero(ctfComponent);
        break;
      }

      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroup(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD: {
        component = this.mapCard(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  private mapSideNavigation(ctfSideNavigation: any) {
    const { anchors } = ctfSideNavigation.fields;

    return {
      name: this.componentNames.SIDE_NAVIGATION,
      slot_enabled: true,
      links: anchors.map((anchor) => {
        return {
          title: anchor.fields.linkText,
          href: anchor.fields.anchorLink,
        };
      }),
    };
  }

  private mapHero(ctfHero: any) {
    const {
      title,
      subheader,
      description,
      video,
      primaryCta,
      backgroundImage,
    } = ctfHero.fields;

    return {
      name: this.componentNames.HERO,
      data: {
        title,
        subtitle: subheader,
        description,
        aos_animation: 'fade-down',
        aos_duration: 500,
        title_variant: 'display1',
        mobile_title_variant: 'heading1-bold',
        img_animation: 'zoom-out-left',
        img_animation_duration: 1600,
        primary_btn: {
          url: primaryCta.fields.externalUrl,
          text: primaryCta.fields.text,
          data_ga_name: primaryCta.fields.dataGaName,
          data_ga_location: primaryCta.fields.dataGaLocation,
        },
        image: {
          bordered: true,
          is_video_thumbnail: true,
          image_url: backgroundImage.fields.file.url,
        },
        modal_src: video.fields.url,
      },
      copy: {
        name: this.componentNames.COPY,
        data: {
          block: [
            {
              subtitle: description,
              column_size: 8,
              hide_horizontal_rule: true,
              margin_bottom: 32,
              subtitle_mobile_padding: true,
            },
          ],
        },
      },
    };
  }

  private mapCardGroup(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.fields.componentName.trim()) {
      case this.componentIds.BENEFITS:
        component = {
          name: 'div',
          id: ctfCardGroup.fields.componentName,
          slot_enabled: true,
          slot_content: [
            {
              name: this.componentNames.FEATURED,
              data: {
                title: ctfCardGroup.fields.header,
                cards: [
                  {
                    title: ctfCardGroup.fields.subheader,
                    rows: ctfCardGroup.fields.card.map((card) => {
                      return {
                        icon: {
                          name: card.fields.iconName,
                        },
                        title: card.fields.title,
                        text: card.fields.subtitle,
                      };
                    }),
                  },
                ],
              },
            },
          ],
        };

        break;

      case this.componentIds.RECOGNITIONS:
        component = {
          name: 'div',
          id: ctfCardGroup.fields.componentName,
          slot_enabled: true,
          slot_content: [
            {
              name: this.componentNames.REPORT_CTA,
              data: {
                title: ctfCardGroup.fields.internalName,
                bg_dark: ctfCardGroup.fields.customFields.bg_dark,
                reports: ctfCardGroup.fields.card.map((card) => {
                  return {
                    description: card.fields.description,
                    url: card.fields.button.fields.externalUrl,
                  };
                }),
              },
            },
          ],
        };
        break;

      case this.componentIds.CAPABILITIES:
        component = {
          name: 'div',
          id: ctfCardGroup.fields.componentName,
          isDarkMode: true,
          slot_enabled: true,
          slot_content: [
            {
              name: this.componentNames.SHOWCASE_FAQ,
              data: {
                content_items: [
                  {
                    title: ctfCardGroup.fields.header,
                    id: 'opti',
                    dropdownItems: ctfCardGroup.fields.card.map((card) => {
                      return {
                        title: card.fields.title,
                        video: {
                          url: card.fields.video.fields.url,
                          thumbnail: card.fields.image.fields.file.url,
                        },
                        text: card.fields.description,
                      };
                    }),
                  },
                ],
                dark_bg: ctfCardGroup.fields.customFields.dark_bg,
              },
            },
          ],
        };

        break;

      case this.componentIds.RESOURCES:
        component = {
          name: this.componentNames.RESOURCE_CAROUSEL,
          slot_enabled: true,
          id: ctfCardGroup.fields.componentName,
          data: {
            column_size: 4,
            title: ctfCardGroup.fields.header,
            cards: ctfCardGroup.fields.card.map((card) => {
              const icon = card.fields.iconName;
              const isVideo = !!card.fields.video;

              const newCard = {
                header: card.fields.title,
                event_type: icon.charAt(0).toUpperCase() + icon.slice(1),
                link_text: card.fields.button?.fields.text,
                data_ga_name: card.fields.button?.fields.dataGaName,
                data_ga_location: card.fields.button?.fields.dataGaLocation,
                icon: {
                  name: icon,
                  variant: 'marketing',
                },
              };

              if (isVideo) {
                return {
                  ...newCard,
                  href: card.fields.video?.fields.url,
                  image:
                    card.fields.video?.fields.thumbnail?.fields.image.fields
                      .file.url,
                };
              }

              return {
                ...newCard,
                href: card.fields.button?.fields.externalUrl,
                image: card.fields.image?.fields.file.url,
              };
            }),
          },
        };
        break;

      default:
        break;
    }
    return component;
  }

  private mapCard(ctfCard: any) {
    const { title, subtitle, button, secondaryButton, image, contentArea } =
      ctfCard.fields;

    return {
      name: 'div',
      id: ctfCard.fields.componentName,
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.CUSTOMER_SUCCESS_CARD,
          data: {
            header: title,
            customers_cta: true,
            case_study: {
              logo_url: contentArea[0].fields.image?.fields.file.url,
              institution_name: ctfCard.fields.list[0],
              content: {
                case_url: button.fields.externalUrl,
                text: subtitle,
                img_url: image.fields.file.url,
                img_alt: image.fields.title,
              },
            },
            cta: {
              href: secondaryButton.fields.externalUrl,
              text: secondaryButton.fields.text,
              data_ga_name: secondaryButton.fields.dataGaName,
            },
          },
        },
      ],
    };
  }
}
