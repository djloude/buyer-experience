# Creating a new application

Our `/partners/technology-partners` page now lives in Contentful CMS. The Page entry for that URL lives [here](https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/entries/1DMq9QYIK5sKBXVyzpE9gv).

To create a new partner application entry, you can use our `Partner Application` content type. There is a checkbox there to mark whether an application should be featured or not.

Featured partners are rendered automatically on the page. To add an application to a category, go back to the Page entry, find the `Block Group` entry with the matching category and add your application entry to it.

## Adding a new category

To create a new category, you can add a `Block Group` entry and add necessary information and references to that category's applications.

You will also need to go into the `JSON Editor` within the Page entry and add a new hyperlink in the buttons section. A DEX engineer will be happy to help if needed!

Any icon from [Slippers Gallery](https://gitlab-com.gitlab.io/marketing/digital-experience/slippers-ui/?path=/story/foundations-icons--gallery) can be used assigning the name of the icon to the new category.

ex:
```
"navigation": {
        "buttons": [
            {
                "href": "#cloud-partners",
                "text": "Cloud Partners",
                "icon_left": "cloud-thin",
                "icon_right": "arrow-down"
            },
            {
                "href": "#code-quality",
                "text": "Code Quality",
                "icon_left": "digital-transformation",
                "icon_right": "arrow-down"
            },
            // ... etc
        ]
}
```